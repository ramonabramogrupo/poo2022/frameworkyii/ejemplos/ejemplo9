<?php

namespace app\models;

use yii\base\Model;

class Formulario5 extends Model{
    // propiedades para almacenar los numeros
    public ?int $n1=null;
    public ?int $n2=null;
    public ?int $n3=null;
    // propiedades para almacenar resultados
    private int $suma=0;
    private int $producto=0;
    private int $resta=0;
    private float $cociente=0;
    
    
    public function attributeLabels(): array {
        return [
            "n1" => "Numero 1",
            "n2" => "Numero 2",
            "n3" => "Numero 3",
        ];
    }
    
    public function rules(): array {
        return [
            [['n1','n2','n3'],'required'],
            [['n1','n2','n3'],'integer']
        ];
    }
    
    public function getSuma(){
        return $this->n1 + $this->n2 + $this->n3;
    }
    
    public function getProducto(){
        return $this->n1 * $this->n2 * $this->n3;
    }
    
    public function getResta(){
        return $this->n1 - $this->n2 - $this->n3;
    }
    
    public function getCociente(){
        return $this->n1 / $this->n2 / $this->n3;
    }
    
}

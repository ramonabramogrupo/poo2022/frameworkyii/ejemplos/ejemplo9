<?php

namespace app\models;

use yii\base\Model;

class Formulario6 extends Model{
    public ?string $nombre=null;
    private int $numeroA=0;
    private int $numeroE=0;
    private int $numeroI=0;
    private int $numeroO=0;
    private int $numeroU=0;
    
    public function attributeLabels(): array {
        return [
          "nombre" => "Introduce nombre",
        ];
    }
    
    public function rules(): array {
        return [
          [['nombre'],'required'],  
        ];
    }
    
    public function getNumeroA(): int {
        return substr_count(strtolower($this->nombre), "a");
    }

    public function getNumeroE(): int {
        return substr_count(strtolower($this->nombre), "e");
    }

    public function getNumeroI(): int {
        return substr_count(strtolower($this->nombre), "i");
    }

    public function getNumeroO(): int {
        return substr_count(strtolower($this->nombre), "o");
    }

    public function getNumeroU(): int {
        return substr_count(strtolower($this->nombre), "u");
    }


    
    
}

<?php

namespace app\models;


class Formulario1 extends \yii\base\Model{
    public ?int $numero1=null;
    public ?int $numero2=null;
    
    public function attributeLabels(): array {
        return [
            "numero1"=>"Introduce el Numero 1",
            "numero2"=>"Introduce el Numero 2"
        ];
    }
    
    public function rules(): array {
        return [
            [['numero1','numero2'],'required'],
            [['numero1','numero2'],'integer'],
        ];
    }
    
}

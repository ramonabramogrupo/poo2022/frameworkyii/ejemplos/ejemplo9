<?php

namespace app\models;

use yii\base\Model;

class Formulario4 extends Model{
    // propiedades para almacenar los numeros
    public ?int $n1=null;
    public ?int $n2=null;
    public ?int $n3=null;

    
    public function attributeLabels(): array {
        return [
            "n1" => "Numero 1",
            "n2" => "Numero 2",
            "n3" => "Numero 3",
        ];
    }
    
    public function rules(): array {
        return [
            [['n1','n2','n3'],'required'],
            [['n1','n2','n3'],'integer']
        ];
    }
    
    public function suma(){
        return $this->n1 + $this->n2 + $this->n3;
    }
    
    public function producto(){
        return $this->n1 * $this->n2 * $this->n3;
    }
    
    public function resta(){
        return $this->n1 - $this->n2 - $this->n3;
    }
    
    public function cociente(){
        return $this->n1 / $this->n2 / $this->n3;
    }
    
}

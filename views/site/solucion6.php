<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    "model" => $model,
    "attributes" => [
        "nombre",
        "numeroA",
        "numeroE",
        "numeroI",
        "numeroO",
        "numeroU",
    ]
]);

?>

<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    "model" => $model,
]);

?>

<ul>
    <li>La suma es <?= $suma ?></li>
    <li>La resta es <?= $resta ?></li>
    <li>El producto es <?= $producto ?></li>
    <li>El cociente es <?= $cociente ?></li>
</ul>

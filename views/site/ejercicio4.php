<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario4 $model */
/** @var ActiveForm $form */
?>
<div class="site-ejercicio4">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'n1') ?>
        <?= $form->field($model, 'n2') ?>
        <?= $form->field($model, 'n3') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-ejercicio4 -->

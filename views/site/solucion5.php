<?php

use yii\widgets\DetailView;

echo DetailView::widget([
    "model" => $model,
    "attributes" => [
        "n1",
        "n2",
        "n3",
        "suma",
        "resta",
        "producto",
        "cociente"
    ]
]);

?>

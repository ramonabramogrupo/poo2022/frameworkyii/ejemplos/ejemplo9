<?php

namespace app\controllers;

use app\models\Formulario1;
use app\models\Formulario2;
use app\models\Formulario3;
use app\models\Formulario4;
use app\models\Formulario5;
use app\models\Formulario6;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionSumar() {
        // creando un modelo vacio del formulario
        // creando un objeto de tipo Formulario1
        $model = new Formulario1();

        // leer los datos del formulario en PHP
        // var_dump($_POST); 
        // leer los datos del formulario desde el objeto request del framework
        // var_dump(Yii::$app->request->post()); 

        $resultado = 0;

        // compruebo si ha pulsado el boton de sumar
        if (Yii::$app->request->post()) {
            // relleno el modelo con todos los datos del formulario
            $model->load(Yii::$app->request->post());

            // sumar los dos numeros
            $resultado = $model->numero1 + $model->numero2;
        }

        // carga la vista sumar 
        // le paso como argumento el objeto de tipo Formulario1
        // le paso un modelo vacio
        return $this->render('sumar', [
                    'model' => $model,
                    'resultado' => $resultado
        ]);
    }

    public function actionMultiplicar() {
        // modelo vacio 
        // para rellenar con los datos del formulario
        $model = new Formulario2();

        // coloco el resultado a 0
        $resultado = 0;

        // compruebo si he pulsado multiplicar
        if (Yii::$app->request->post()) {
            // leo los datos del formulario y 
            // los cargo al modelo
            $model->load(Yii::$app->request->post());
            $resultado = $model->numero1 * $model->numero2;
        }

        // llamo a la vista
        // le paso el modelo
        // y el resultado
        return $this->render("multiplicar", [
                    "model" => $model,
                    "resultado" => $resultado
        ]);
    }

    public function actionEjercicio3() {
        // cargando el modelo vacio
        $model = new Formulario3();

        // compruebo que he pulsado el boton de concatenar
        if ($model->load(Yii::$app->request->post())) {
            // compruebo que cumpla las reglas del modelo
            if ($model->validate()) {
                // aqui es donde realizo la operacion
                // en este caso concateno
                $resultado = $model->concatenar();
                // llamo a la vista para mostrar el resultado
                return $this->render("solucion3", [
                            "resultado" => $resultado,
                            "model" => $model
                ]);
            }
        }

        return $this->render('ejercicio3', [
                    'model' => $model,
        ]);
    }
    
     public function actionEjercicio4() {
        // modelo vacio
        $model = new Formulario4();

        // si has pulsado el boton de enviar
        if ($model->load(Yii::$app->request->post())) {
            // compruebo que los datos del formulario cumplan las reglas
            if ($model->validate()) {
                
                // realizo los calculos
                $s=$model->suma();
                $r=$model->resta();
                $m=$model->producto();
                $d=$model->cociente();
                
                // muestro los resultados en la vista
                return $this->render("solucion4",[
                    "model" => $model,
                    "suma" => $s,
                    "resta" => $r,
                    "producto" => $m,
                    "cociente" => $d
                ]);
            }
        }

        return $this->render('ejercicio4', [
                    'model' => $model,
        ]);
    }

    public function actionEjercicio5() {
        // modelo vacio
        $model = new Formulario5();

        // si has pulsado el boton de enviar
        if ($model->load(Yii::$app->request->post())) {
            // compruebo que los datos del formulario cumplan las reglas
            if ($model->validate()) {
                
                // muestro los resultados en la vista
                return $this->render("solucion5",[
                    "model" => $model,

                ]);
            }
        }

        return $this->render('ejercicio5', [
                    'model' => $model,
        ]);
    }
    
    
//    Ejercicio6 ==> nombre de la accion y el nombre de la vista
//    Formulario6 ==> modelo
//    solucion6 ==> la vista donde muestra resultado
//    
//    introduzco nombre
//    me debe indicar el numero de a,e,i,o,u
            
    public function actionEjercicio6(){
         // modelo vacio
        $model = new Formulario6();

        // si has pulsado el boton de enviar
        if ($model->load(Yii::$app->request->post())) {
            // compruebo que los datos del formulario cumplan las reglas
            if ($model->validate()) {
                
                // muestro los resultados en la vista
                return $this->render("solucion6",[
                    "model" => $model,

                ]);
            }
        }

        return $this->render('ejercicio6', [
                    'model' => $model,
        ]);
        
    }            

}
